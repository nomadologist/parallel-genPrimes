#include <stdio.h>
#include <omp.h>
#include <stdlib.h>

//algorithm function declaration
void findPrimes(int N, int t, int *primes);

//generates primes from 2 to N with t threads
void findPrimes(int N, int t, int *primes)
{
	#pragma omp parallel num_threads(t)
	{
		// variable declarations
		int threads = omp_get_num_threads();
		int j = 0;
		int current;
		int primesCopy[(N-1)/threads];

		//finding multiples with larger numbers takes longer than smaller ones so use static scheduling
		#pragma omp for nowait schedule(static, 1)
		for(int i = 2; i <= N; i++)
		{
			// assigns each thread a portion of the numbers
			primesCopy[j++] = i;
		}
		// loop over the array to remove anything that isn't a prime
		for(int i = 0; i < j; i++)
		{
			current = primesCopy[i];
			// within that, check each number up to the floor specified in problem description
			for(int k = 2; k <= ((N+1)/2); k++)
			{
				// if it has a multiple that isn't itself, the number isn't prime, so we set that part of the array to 0
				if(current % k == 0 && current!=k)
				{
					primesCopy[i] = 0;
					break;
				}
				else if(current <= k)
				{
					break;
				}
			}
		}

		// copy all the primes to the main array
		for(int i = 0; i<j; i++)
		{
			current = primesCopy[i];
			if(current!=0)
			{
				primes[current] = current;
			}
		}
	}
}

int main(int argc, char *argv[])
{
	int N;
	int t;

	double tstart = 0.0, ttaken;
//Part of your program that reads the input from the command line
	N = atoi(argv[1]);
	t = atoi(argv[2]);

	tstart = omp_get_wtime();

//Part of your program that generates the prime numbers (as indicated by the algorithm above)
	int *primes = (int *) calloc(sizeof(int), (N+1));
	findPrimes(N, t, primes);

	ttaken = omp_get_wtime() - tstart;
	printf("Time taken for the main part: %f\n", ttaken);

//Write the output file and exit.
//	int numChars = (int)((ceil(log10(N))+1)*sizeof(char));
	int numChars = 64;
	char name[numChars];
	// generate file name
	sprintf(name, "%d.txt", N);
	// create file in write mode
	FILE *file = fopen(name, "w");

	int rank = 1;
	int previous = 2;
	int current = 2;

	// loop over the array, if it doesn't contain 0, it is a prime number
	for(int i = 2; i<=N; i++)
	{
		if(primes[i]!=0)
		{
			current = i;
			fprintf(file, "%d, %d, %d\n", rank++, current, (current - previous));
			previous = current;
		}
	}

	fclose(file);
	free(primes);
}